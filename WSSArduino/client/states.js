export default {
  checkBoxes: [
    // -------------
    {
      name: 'A0',
      type: 'A00',
      id: 100,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 1001,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 1001,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 1002,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 1002,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 1001,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 1002,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    {
      name: 'A1',
      type: 'A01',
      id: 1,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 11,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 111,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 112,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 12,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 121,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    {
      name: 'A2',
      type: 'A02',
      id: 2,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 21,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 211,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 212,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 22,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 221,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // -------------
    {
      name: 'A3',
      type: 'A03',
      id: 3,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 31,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 31,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 32,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 32,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 31,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 32,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    {
      name: 'A4',
      type: 'A04',
      id: 4,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 41,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 41,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 42,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 42,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 41,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 42,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A5',
      type: 'A05',
      id: 5,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 51,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 51,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 52,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 52,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 51,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 52,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A6',
      type: 'A06',
      id: 6,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 61,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 61,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 62,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 62,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 61,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 62,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A7',
      type: 'A07',
      id: 7,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 71,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 71,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 72,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 72,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 71,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 72,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A8',
      type: 'A08',
      id: 8,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 81,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 81,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 82,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 82,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 81,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 82,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A9',
      type: 'A09',
      id: 9,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 91,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 91,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 92,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 92,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 91,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 92,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A10',
      type: 'A10',
      id: 10,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 102,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 102,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 101,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 102,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A11',
      type: 'A11',
      id: 11,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 111,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 111,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 112,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 112,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 111,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 112,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A12',
      type: 'A12',
      id: 12,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 121,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 121,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 122,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 122,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 121,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 122,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A13',
      type: 'A13',
      id: 13,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 131,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 131,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 132,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 132,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 131,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 132,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A14',
      type: 'A14',
      id: 14,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 141,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 141,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 142,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 142,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 141,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 142,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'A15',
      type: 'A15',
      id: 15,
      vButton: 'L',
      content: '',
      variable: 'N', 
      checkBox: [
        {
          id: 151,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 151,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 152,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
        {
          id: 152,
          value: 'Analog',
          isChecked: false,
          childCheckBox: [
            {
              id: 151,
              value: 'Variable',
              isChecked: false,
            },
            {
              id: 152,
              value: 'Variable',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D0',
      type: 'D00',
      id: 1010,
      vButton: 'L',
      content: '',
      variable: 'N', 
      checkBox: [
        {
          id: 10101,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10101,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10102,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    {
      name: 'D1',
      type: 'D01',
      id: 1011,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10111,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101111,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101112,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    {
      name: 'D2',
      type: 'D02',
      id: 1012,
      vButton: 'L',
      content: '',
      variable: 'N', 
      checkBox: [
        {
          id: 10121,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101211,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101212,
              value: 'Output',
              isChecked: false,
            },
          ], },
      ],
    },
    // -------------
    {
      name: 'D3',
      type: 'D03',
      id: 1013,
      vButton: 'L',
      content: '',
      variable: 'N', 
      checkBox: [
        {
          id: 10131,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10131,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10132,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D4',
      type: 'D04',
      id: 1014,
      vButton: 'L',
      content: '',
      variable: 'N', 
      checkBox: [
        {
          id: 10141,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10141,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10142,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D5',
      type: 'D05',
      id: 1015,
      vButton: 'L',
      content: '',
      variable: 'N', 
      checkBox: [
        {
          id: 10151,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10151,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10152,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D6',
      type: 'D06',
      id: 1016,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10161,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10161,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10162,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D7',
      type: 'D07',
      id: 1017,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10171,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10171,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10172,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D8',
      type: 'D08',
      id: 1018,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10181,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10181,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10182,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D9',
      type: 'D09',
      id: 1019,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10191,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10191,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10192,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D10',
      type: 'D10',
      id: 10110,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101101,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101101,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101102,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D11',
      type: 'D11',
      id: 10111,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101111,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101111,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101112,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D12',
      type: 'D12',
      id: 10112,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101121,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101121,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101122,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D13',
      type: 'D13',
      id: 10113,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101131,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101131,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101132,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D14',
      type: 'D14',
      id: 10114,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101141,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101141,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101142,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D15',
      type: 'D15',
      id: 10115,
      vButton: 'L',
      content: '',
       variable: 'N',
      checkBox: [
        {
          id: 101151,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101151,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101152,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D16',
      type: 'D16',
      id: 10116,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101161,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101161,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101162,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    {
      name: 'D17',
      type: 'D17',
      id: 10117,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101171,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 1011171,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 1011172,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    {
      name: 'D18',
      type: 'D18',
      id: 10118,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101181,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 1012181,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 1012182,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // -------------
    {
      name: 'D19',
      type: 'D019',
      id: 10119,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101191,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101191,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101192,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D20',
      type: 'D20',
      id: 10120,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10141,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 10141,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 10142,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D21',
      type: 'D21',
      id: 10121,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101211,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101211,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101212,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D22',
      type: 'D22',
      id: 10122,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101221,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101221,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101222,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D23',
      type: 'D023',
      id: 10123,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101231,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101231,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101232,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D24',
      type: 'D024',
      id: 10124,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101241,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101241,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101242,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D25',
      type: 'D025',
      id: 10125,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101251,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101251,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101252,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D26',
      type: 'D26',
      id: 10126,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101261,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101261,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101262,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D27',
      type: 'D27',
      id: 10127,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101271,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101271,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101272,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D28',
      type: 'D28',
      id: 10128,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101281,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101281,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101282,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D29',
      type: 'D29',
      id: 10129,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101291,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101291,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101292,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D30',
      type: 'D30',
      id: 10130,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101301,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101301,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101302,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D31',
      type: 'D31',
      id: 10131,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101311,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101311,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101312,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    {
      name: 'D32',
      type: 'D32',
      id: 10132,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 10321,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101321,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101322,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    {
      name: 'D33',
      type: 'D33',
      id: 10133,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101331,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101331,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101332,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    {
      name: 'D34',
      type: 'D34',
      id: 10134,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101341,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101341,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101342,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // -------------
    {
      name: 'D35',
      type: 'D035',
      id: 10135,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101351,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101351,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101352,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D36',
      type: 'D36',
      id: 10136,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101361,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101361,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101362,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D37',
      type: 'D37',
      id: 10137,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101371,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101371,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101372,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D38',
      type: 'D38',
      id: 10138,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101381,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101381,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101382,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D39',
      type: 'D39',
      id: 10139,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101391,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101391,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101392,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D40',
      type: 'D40',
      id: 10140,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101401,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101401,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101402,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D41',
      type: 'D41',
      id: 10141,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101411,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101411,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101412,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D42',
      type: 'D42',
      id: 10142,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101421,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101421,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101422,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D43',
      type: 'D43',
      id: 10143,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101431,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101431,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101432,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D44',
      type: 'D44',
      id: 10144,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101441,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101441,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101442,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D45',
      type: 'D45',
      id: 10145,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101451,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101451,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101452,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D46',
      type: 'D46',
      id: 10146,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101461,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101461,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101462,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D47',
      type: 'D47',
      id: 10147,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101471,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101471,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101472,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D48',
      type: 'D48',
      id: 10148,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101481,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101481,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101482,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    {
      name: 'D49',
      type: 'D49',
      id: 10149,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101491,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 1011491,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 1011492,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    {
      name: 'D50',
      type: 'D50',
      id: 10150,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101501,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 1012501,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 1012502,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // -------------
    {
      name: 'D51',
      type: 'D051',
      id: 10151,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101511,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101511,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101512,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D52',
      type: 'D52',
      id: 10152,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101521,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101521,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101522,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
    // -------------
    {
      name: 'D53',
      type: 'D53',
      id: 101531,
      vButton: 'L',
      content: '',
      variable: 'N',
      checkBox: [
        {
          id: 101531,
          value: 'Digital',
          isChecked: false,
          childCheckBox: [
            {
              id: 101531,
              value: 'Input',
              isChecked: false,
            },
            {
              id: 101532,
              value: 'Output',
              isChecked: false,
            },
          ],
        },
      ],
    },
    // ---- -----
  ],
};
