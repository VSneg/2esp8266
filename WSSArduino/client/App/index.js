import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SimpleButton from '../components/SimpleButton';
import styled from 'styled-components';
import FieldSet from '../components/FieldSet';
import checkBoxesStates from '../states';

// для тестов сокетов на сервере
// const Connection = new WebSocket('ws://' + location.hostname + ':3030/echo');

const Connection = new WebSocket('ws://' + location.hostname + ':81/', [
  'arduino',
]);

const GroupCheckBox = styled.div`
   {
    width: 200px;
    margin-left: 20px;
  }
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = checkBoxesStates;
  }

  onChangeChild = idsObject => {
    let states = this.state;
    let checkBoxes = states.checkBoxes.find(
      obj => obj.id == idsObject.globalId,
    );
    let checkBox = checkBoxes.checkBox.find(obj => obj.id == idsObject.checkId);
    let childCheckBox = checkBox.childCheckBox.find(
      obj => obj.id == idsObject.checkBoxId,
    );
    if (childCheckBox.isChecked) {
      childCheckBox.isChecked = false;
    } else {
      checkBox.childCheckBox.map(obj => {
        obj.isChecked = false;
      });
      childCheckBox.isChecked = true;
    }

    console.log(childCheckBox.value[0], 'ls');

    if (childCheckBox.value[0] === 'I') checkBoxes.variable = 'I';
    if (childCheckBox.value[0] === 'O') checkBoxes.variable = 'O';
    if (childCheckBox.value[0] === 'V') checkBoxes.variable = 'V';

    this.setState({
      state: states,
    });
  };

  onChangeCheckBox = idsObject => {
    let states = this.state;
    let checkBoxes = states.checkBoxes.find(
      obj => obj.id == idsObject.globalId,
    );
    let checkBox = checkBoxes.checkBox.find(
      obj => obj.id == idsObject.checkBoxId,
    );
    if (checkBox.isChecked) {
      checkBox.isChecked = false;
    } else {
      checkBoxes.checkBox.map(obj => {
        obj.isChecked = false;
      });
      checkBox.isChecked = true;
    }

    this.setState({
      state: states,
    });
  };

  initWebsocket() {
    console.log('Connection');
    Connection.onopen = function() {
      console.log('Connect ' + new Date());
    };

    Connection.onmessage = evt => {
      let states = this.state;
      let checkBoxes = states.checkBoxes.find(
        obj => obj.type == evt.data.slice(0, 3),
      );

      console.log('message: ' + evt.data);
      const _data = String(evt.data);
      if (typeof checkBoxes !== 'undefined') {
        if (checkBoxes.variable === 'I') {
          (checkBoxes.vButton = _data.slice(3, _data.length)),
            this.setState({
              state: states,
            });
        }
        if (checkBoxes.variable === 'V') {
          (checkBoxes.content = _data.slice(3, _data.length)),
            this.setState({
              state: states,
            });
        }
      }
    };

    Connection.onerror = function(error) {
      console.log('WebSocket Error ', error);
    };

    Connection.onclose = () => {
      console.log('Websocket disconnected');
    };
  }

  componentDidMount() {
    this.initWebsocket();
  }

  getVariables(globalId) {
    let sendStr = '';
    const states = this.state;
    const checkBoxes = states.checkBoxes.find(obj => obj.id == globalId);

    const checkBox = checkBoxes.checkBox.find(obj => obj.isChecked === true);
    const childCheckBox = checkBox.childCheckBox.find(
      obj => obj.isChecked === true,
    );

    sendStr += checkBoxes.type;
    sendStr += checkBox.value[0];
    if (childCheckBox.value[0] !== 'V') {
      sendStr += 'L';
      sendStr += childCheckBox.value[0];
    } else {
      sendStr += 'VI';
    }

    return sendStr;
  }

  vButton = globalId => {
    let states = this.state;
    let checkBoxes = states.checkBoxes.find(obj => obj.id == globalId);
    if (checkBoxes.vButton == 'L') {
      checkBoxes.vButton = 'H';
    } else {
      checkBoxes.vButton = 'L';
    }

    const checkBox = checkBoxes.checkBox.find(obj => obj.isChecked === true);
    const childCheckBox = checkBox.childCheckBox.find(
      obj => obj.isChecked === true,
    );

    console.log(
      'send: ' +
        checkBoxes.type +
        checkBox.value[0] +
        checkBoxes.vButton +
        childCheckBox.value[0],
    );

    Connection.send(
      String(
        '@' +
          checkBoxes.type +
          checkBox.value[0] +
          checkBoxes.vButton +
          childCheckBox.value[0],
      ),
    );

    this.setState({
      state: states,
    });
  };

  saveButton = globalId => {
    console.log('send: ' + this.getVariables(globalId));
    Connection.send(String('@' + this.getVariables(globalId)));
  };

  render() {
    return (
      <div>
        <GroupCheckBox>
          {this.state.checkBoxes.map((i, index) => {
            return (
              <React.Fragment key={index}>
                <span>{`Pin Number ${i.name}`}</span>
                {i.variable !== 'V' && (
                  <SimpleButton
                    value={`${i.type} / ${i.vButton}`}
                    onClick={this.vButton.bind(this, i.id)}
                    disabled={
                      i.variable === 'N'
                        ? true
                        : false || i.variable === 'I'
                        ? true
                        : false
                    }
                  />
                )}
                {i.variable === 'V' && <input type="text" value={i.content} />}

                <FieldSet
                  checkBox={i}
                  onChange={this.onChangeCheckBox}
                  onChangeChild={this.onChangeChild}
                  saveButton={this.saveButton}
                />
              </React.Fragment>
            );
          })}
        </GroupCheckBox>
      </div>
    );
  }
}

export default App;
