import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SimpleButton from '../SimpleButton';
import styled from 'styled-components';

class FieldSet extends Component {
  static propTypes = {
    states: PropTypes.object,
    onChange: PropTypes.func,
  };

  render() {
    const {checkBox, globalId, checkId} = this.props;
    return (
      <div>
        <fieldset style={{width: '150px'}}>
          {checkBox.map((i, index) => {
            return (
              <React.Fragment key={index}>
                <input
                  type="checkbox"
                  onChange={() =>
                    this.props.onChange({
                      globalId: globalId,
                      checkId: checkId,
                      checkBoxId: i.id,
                    })
                  }
                  checked={i.isChecked}
                />
                <label>{i.value}</label>
              </React.Fragment>
            );
          })}
        </fieldset>
      </div>
    );
  }
}

export default FieldSet;
