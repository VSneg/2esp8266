import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SimpleButton from '../SimpleButton';
import FieldSetChild from '../FileSetChild';
import styled from 'styled-components';

const FlexBlock = styled.div`
   {
    padding: 0;
    margin: 0;
    list-style: none;
    -ms-box-orient: horizontal;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -moz-flex;
    display: -webkit-flex;
    display: flex;
  }
`;

const CheckBox = styled.div`
   {
    width: 200px;
    padding-bottom: 20px;
  }
`;

class FieldSet extends Component {
  static propTypes = {
    states: PropTypes.object,
    onChange: PropTypes.func,
    saveButton: PropTypes.func,
  };

  render() {
    const {checkBox} = this.props;

    return (
      <FlexBlock>
        <CheckBox>
          <fieldset>
            {checkBox.checkBox.map((i, index) => {
              return (
                <React.Fragment key={i.id}>
                  <input
                    type="checkbox"
                    onChange={() =>
                      this.props.onChange({
                        globalId: checkBox.id,
                        checkBoxId: i.id,
                      })
                    }
                    checked={i.isChecked}
                  />
                  <label>{i.value}</label>
                </React.Fragment>
              );
            })}
          </fieldset>

          {checkBox.checkBox.map((i, index) => {
            if (!i.isChecked) return null;

            return (
              <div>
                <FieldSetChild
                  key={i.id}
                  checkBox={i.childCheckBox}
                  checkId={i.id}
                  globalId={checkBox.id}
                  onChange={this.props.onChangeChild}
                />
                <SimpleButton
                  value="Save"
                  onClick={this.props.saveButton.bind(this, checkBox.id)}
                />
              </div>
            );
          })}
        </CheckBox>
      </FlexBlock>
    );
  }
}

export default FieldSet;
