import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Button = styled.button`
  background: transparent;
  width: 150px;
  font-size: 10pt;
  border-radius: 3px;
  border: 2px solid grey;
  color: grey;
  margin: 0 2px;
  padding: 0.25em 1em;
	&:active {
		background: #ccc;
		color: #fff;
	}
`;

class SimpleButton extends Component {
	static propTypes = {
    value: PropTypes.string,
		onClick: PropTypes.func,
    disable: PropTypes.bool,
  };
  render() {
    return <Button onClick={this.props.onClick} disabled={this.props.disabled}>{this.props.value}</Button>;
  }
}

export default SimpleButton;
