import app from './app';

const { PORT = 3030 } = process.env;
app.listen(PORT, () => console.log(`Listening on port ${PORT}`)); 
