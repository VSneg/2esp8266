import express from 'express';
import path from 'path';
import proxy from 'express-http-proxy';
import expressWss from 'express-ws';

const app = express();
const expressWs = expressWss(app);

app.ws('/echo', function(ws, req) {
  const runWs = () => {
    ws.send('A00H');
    console.log('send')
    setTimeout(() => {
      runWs();
    }, 6000);
  };

  ws.on('message', function(msg) {
    console.log(msg);
    runWs();
  });
});

app.use(express.static(path.join(__dirname, '../public/')));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/api', proxy('http://localhost:3000/'));
export default app;
