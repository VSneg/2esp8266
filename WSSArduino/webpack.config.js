var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    index: './client/index.js',
    vendor: ['styled-components'],
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].bundle.js',
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['es2015', 'react', 'stage-2'],
          cacheDirectory: true,
          plugins: [
            [
              'babel-plugin-styled-components',
              {
                transpileTemplateLiterals: false,
              },
            ],
          ],
        },
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.hbs/,
        loader: 'handlebars-template-loader',
      },
    ],
  },
};
