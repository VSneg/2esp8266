'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _expressHttpProxy = require('express-http-proxy');

var _expressHttpProxy2 = _interopRequireDefault(_expressHttpProxy);

var _expressWs = require('express-ws');

var _expressWs2 = _interopRequireDefault(_expressWs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express2.default)();
const expressWs = (0, _expressWs2.default)(app);

app.ws('/echo', function (ws, req) {
  ws.on('message', function (msg) {
    // ws.send(msg);
    console.log(msg);
  });
  ws.send("test");
});

app.use(_express2.default.static(_path2.default.join(__dirname, '../public/')));
app.get('/', function (req, res) {
  res.sendFile(_path2.default.join(__dirname + '/index.html'));
});

app.use('/api', (0, _expressHttpProxy2.default)('http://localhost:3000/'));
exports.default = app;
//# sourceMappingURL=app.js.map