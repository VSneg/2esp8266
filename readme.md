# Установка фронта
## устанавливаем webpack 

глобально:

```npm install webpack -g```

или локально:

```npm install webpack```

так же необходимо установить webpack-cli, при установке webpack, необходимо ответить yes.

## устанавливаем yarn 
Этот пункт является необязательным, пакеты можно установить и через npm

https://yarnpkg.com/lang/en/docs/install/#debian-stable


## установка пакетов
переходим в папку WSSArduino/

вводим команду:

```yarn install```

или

```npm install``` 

## запуск проекта

запуск сервера

```npm run server```

для запуска сборки

```npm run webpack:build``` - для продакшена, с минификацией js бандла

```npm run webpack:dev``` - для девелопа 


